# Overview

A set of actions, reducers, and selectors that sets up the infrastructure for handling pagination in redux.

The goals of this package are:
- Provide a uniform interface for writing/reading paginated content to/from a redux store.
- Handling multiple instances of paged content in a single defined reducer (see Sliced Pagination below)

## Reducers

The reducer supplied by redux-pageable is meant to hook in the root of your redux store state. Multiple reducers can be generated for multiple types of pagination. For example, if your application had a list of football teams and list of basketball teams, you can create a redux-pageable reducer specific for each and independently hook them up to the redux store.

## Actions

The actions supplied by redux-pageable are used to request a page from a remote api. There are a handful of actions that allow for management of the paginated redux state as well.

Note: redux-pageable makes no assumptions about how you make remote api calls. A user supplied async function is used for that communication.

## Selectors

The selectors supplied by redux-pageable are simply a convenience for accessing the paginated data being stored by the reducers.

# Installation

1. `yarn add @panderasystem/redux-pageable`

## Compatibility

### regeneratorRuntime

This package outputs code with support for compatibility with the latest 2 versions of browsers. However, it does not include any polyfills and requires the inclusion of `regeneratorRuntime` by the application using this package.

1. `yarn add regeneratorRuntime`
2. add `import 'regenerator-runtime/runtime';` to the top of your entry file.

### redux-thunk

Internally, redux-pageable uses thunk actions and thus requires redux-thunk is configured as a middleware.

# Usage

Three factory functions are used to generate the three essential pieces of redux-pageable.

## Use the factory functions

### Reducers
```
import { makePaginationReducer } from '@panderasystems/redux-pageable';
...
const groceriesPageableReducer = makePaginationReducer('groceriesPageableReducer');

// standard reducer composition
const store = createStore({ groceriesPageableReducer }, initialState, middleware);
```

#### makePaginationReducer

TODO - document factory function

### Actions
```
import { makePaginationActions } from '@panderasystems/redux-pageable';
...

// create actions to control the groceries list
const {
  actionTypes,
  clearErrors,
  clearPages,
  fetchPage,
  reset,
} = makePaginationActions('groceriesPageableReducer', ({ pageOptions, reducerId, instanceId }) => async (dispatch, getState) => {
  // make your remote call here
  // note that you can dispatch any other custom redux actions you'd like here.
  const response = await fetch(`/groceries?page=${pageOptions.page}&size=${pageOptions.size}`);
  // manipulate the response before redux-pageable gets it, if you'd like
  return response;
  });

...

// example call to retrieve the first page of data
dispatch(fetchPage({
  page: 0,
  size: 50,
  }));
...

```
#### makePaginationActions

TODO - document factory function

#### fetchPage

TODO - document action

#### clearErrors

TODO - document action

#### clearPages

TODO - document action

#### reset

TODO - document action

### Selectors
```
import { makePaginationSelectors } from '@panderasystems/redux-pageable';

const {
  selectActivePage,
  selectActivePageNumber,
  selectError,
  selectIsLoading,
  selectOptions,
  selectPages,
  selectTotals
} = makePaginationSelectors('groceriesPageableReducer');

// in a React Component
class Groceries extends Component {
  ...
  render() {
    if (this.props.loading) {
      return (<span>The groceries list is loading, please wait.</span>);
    }

    if (this.props.error) {
      return (<span>Oh now we have an error. Details: {error}</span>);
    }

    return (
      <div>
        <span>currently viewing page ${activePage.pageNumber} of a total ${totals.totalPages}</span>
        <ul>
          {/*
            Important to note that the structure of the response is left up to the consumer.
            redux-pageable makes no assumptions about the shape of a page or the items within it.
          */}
          {activePage.response.groceryItems.map(groceryItem => <li>{groceryItem.title}</li>)}
        </ul>
      </div>
    )
  }
  ...
}
...
function mapStateToProps(state) {
  error: selectError(state),
  loading: selectIsLoading(state),
  activePage: selectActivePage(state),
  totals: selectTotals(state),
}
```

#### selectActivePage

TODO - document selector

#### selectActivePageNumber

TODO - document selector

#### selectError

TODO - document selector

#### selectIsLoading

TODO - document selector

#### selectOptions

TODO - document selector

#### selectPages

TODO - document selector

#### selectTotals

TODO - document selector

## Advanced Concepts

### How redux-pageable caches pages

TODO

### Sliced Pagination

A common pattern in mobile applications is stacked navigation. Say you have an app that allows you to view a playlist of songs. When you tap into a particular song, it shows you other playlists that song is included on. Now you tap into one of those playlists. The navigation structure currently looks like this:

Playlist A -> Song Z -> Playlist B

Playlist A is still hanging around in the stack, and you if were to go back up to it, you would expect that it still contains its songs in that playlist.

Redux isn't great a modeling this given the centralized nature of redux data storage. This is why redux-pageable allows for multiple instances of paginated data under the same reducer.

```
// structure of state stored by redux-pageable when there are multiple instances
playlistPageableReducer: {
  instances: {
    'playlistA-ID': { ... }, // all Playlist A paginated data siloed
    'playlistB-ID': { ... }, // and the same for Playlist B
  }
}
```

TODO - show how the supplied actions and selectors target a particular pageable instance

TODO - describe how 'defaultinstance' is used when no instanceid is specified in actions and selectors

### Storing Responses elsewhere in redux

If you're using redux, you may be normalizing your data. That might entail having reducer wells that are simply responsible for storing a piece of data that is used in several parts of your UI. While redux-pageable stores the entire api response in the redux-pageable reducer state, there are a couple of locations where you can push that data elsewhere.
1. In your custom api method that the `makePaginationActions` factory function takes, you are making the api call and thus, can dispatch an action from there to save the data in another part of your redux state.
2. makePaginationActions also returns an `actionTypes` object that contains an string entry for each action type. In some other reducer, just create a case to handle this action: `actionTypes.fetchPageSucceeded`.

Now that your data is also stored elsewhere in your redux state, you'll still need to access it in a paginated context. That's where you'll create a custom selector that uses `selectPages` or `selectActivePage` as an input along with a selector that grabs the data you stored somewhere else, then combines the two for your final paginated output.
