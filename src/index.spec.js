// @flow
// not including polyfills in the build output, but need them to run tests
import '@babel/polyfill';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
  makePaginationActions,
  makePaginationReducer,
  makePaginationSelectors,
} from './index';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
const mockDispatchable = () => async () => Promise.resolve({});

describe('Default reducer instance usecase', () => {
  describe('Simple actions and reducer integration', () => {
    let paginationReducer;
    let paginationActions;

    beforeEach(() => {
      paginationReducer = makePaginationReducer();
      paginationActions = makePaginationActions(mockDispatchable);
    });

    it('action fetchPageStarted should update the fetching status', () => {
      const nextState = paginationReducer(undefined, paginationActions.fetchPageStarted({
        pageOptions: { page: 0 },
      }));
      expect(nextState.instances.defaultinstance.pages['0'].fetching).toEqual(true);
    });

    it('action fetchPageFailed should update the fetching status', () => {
      const nextState = paginationReducer(undefined, paginationActions.fetchPageFailed({
        pageOptions: { page: 0 },
        error: { errKey: 'errValue' },
      }));
      expect(nextState.instances.defaultinstance.pages['0'].fetching).toEqual(false);
      expect(nextState.instances.defaultinstance.pages['0'].error).toEqual({ errKey: 'errValue' });
    });

    it('action fetchPageStarted should not reset the error value', () => {
      const failedState = paginationReducer(undefined, paginationActions.fetchPageFailed({
        pageOptions: { page: 0 },
        error: { errKey: 'errValue' },
      }));
      expect(failedState.instances.defaultinstance.pages['0'].error).toEqual({ errKey: 'errValue' });
      const nextState = paginationReducer(failedState, paginationActions.fetchPageStarted({
        pageOptions: { page: 0 },
      }));
      expect(nextState.instances.defaultinstance.pages['0'].error).toEqual({ errKey: 'errValue' });
    });

    it('action fetchPageSucceeded should update the reducer page results and instance state for defaultinstance page 0', () => {
      const mockPage = {
        ids: ['1', '2', '3'],
        number: 0,
        totalPages: 2,
        totalElements: 5,
        numberOfElements: 3,
        first: true,
        last: false,
        index: {
          '1a': { hello: 'first-result' },
          '2a': { hello: 'second-result' },
          '3a': { hello: 'third-result' },
        },
      };
      // set the initial state to be in the started position
      const stateAfterStarted = paginationReducer(undefined, paginationActions.fetchPageStarted({
        pageOptions: { page: 0 },
      }));
      const nextState = paginationReducer(stateAfterStarted, paginationActions.fetchPageSucceeded({
        pageOptions: { page: 0 },
        response: mockPage,
      }));
      // stored as the first page since its number: 0
      expect(nextState.instances.defaultinstance.pages['0'].response).toEqual(mockPage);
      // only one page should exist
      expect(Object.keys(nextState.instances.defaultinstance.pages).length).toEqual(1);
      // totalElements and totalPages are stored as a sibling to pages
      expect(nextState.instances.defaultinstance.totalElements).toEqual(5);
      expect(nextState.instances.defaultinstance.totalPages).toEqual(2);
      // its not fetching anymore
      expect(nextState.instances.defaultinstance.pages['0'].fetching).toEqual(false);
    });

    it('action fetchPageSucceeded should update the reducer page results for defaultinstance page 4 and page 5', () => {
      const mockPage4 = {
        ids: ['page4result1', 'page4result2'],
        number: 4, // note this number does not matter, the PageOptions passed in determine page number resp is stored at
        totalPages: 10,
        totalElements: 20,
        numberOfElements: 2,
        first: false,
        last: false,
        index: {
          page4result1: { hello: 'page4result1' },
          page4result2: { hello: 'page4result2' },
        },
      };
      const mockPage5 = {
        ids: ['page5result1', 'page5result2'],
        number: 5,
        totalPages: 10,
        totalElements: 20,
        numberOfElements: 2,
        first: false,
        last: false,
        index: {
          page5result1: { hello: 'page5result1' },
          page5result2: { hello: 'page5result2' },
        },
      };
      const stateAfterPage4 = paginationReducer(
        undefined,
        paginationActions.fetchPageSucceeded({
          pageOptions: { page: 4 },
          response: mockPage4,
        }),
      );
      const stateAfterPage5 = paginationReducer(
        stateAfterPage4,
        paginationActions.fetchPageSucceeded({
          pageOptions: { page: 5 },
          response: mockPage5,
        }),
      );

      // pages are stored at the correct location in the reducer state
      expect(stateAfterPage5.instances.defaultinstance.pages['4'].response).toEqual(mockPage4);
      expect(stateAfterPage5.instances.defaultinstance.pages['5'].response).toEqual(mockPage5);
      // two pages should exist
      expect(Object.keys(stateAfterPage5.instances.defaultinstance.pages).length).toEqual(2);
    });

    it('action fetchPageSucceeded should update the reducer page results for a non default instance', () => {
      const instanceId = 'custominstance';
      const mockPage = {
        ids: ['1', '2', '3'],
        number: 0,
        totalPages: 2,
        totalElements: 5,
        numberOfElements: 3,
        first: true,
        last: false,
        index: {
          '1a': { hello: 'first-result' },
          '2a': { hello: 'second-result' },
          '3a': { hello: 'third-result' },
        },
      };
      const nextState = paginationReducer(
        undefined,
        paginationActions.fetchPageSucceeded({
          pageOptions: { page: 0 },
          response: mockPage,
          instanceId,
        }),
      );
      // stored as the first page of our custominstance
      expect(nextState.instances[instanceId].pages['0'].response).toEqual(mockPage);
      // default instance is left untouched
      expect(Object.keys(nextState.instances.defaultinstance.pages).length).toEqual(0);
    });

    it('action fetchPageSucceeded should clear an existing error', () => {
      // TODO
    });
  });

  describe('fetchPage thunk action', () => {
    function customRequestCall({ pageOptions, instanceId, reducerId }) {
      return async () => {
        console.log('calling custom request', pageOptions, instanceId, reducerId);
        // Can use getState to get something like jwt headers
        // Can call any other dispatches desired
        return new Promise((resolve) => {
          if (pageOptions.page === 0) {
            resolve({
              ids: ['1a', '2a', '3a'],
              number: 0,
              totalPages: 2,
              totalElements: 5,
              numberOfElements: 3,
              first: true,
              last: false,
              index: {
                '1a': { hello: 'first-result' },
                '2a': { hello: 'second-result' },
                '3a': { hello: 'third-result' },
              },
            });
          } else if (pageOptions.page === 1) {
            resolve({
              ids: ['4a', '5a'],
              number: 1,
              totalPages: 2,
              totalElements: 5,
              numberOfElements: 2,
              first: false,
              last: true,
              index: {
                '4a': { hello: 'fourth-result' },
                '5a': { hello: 'fifth-result' },
              },
            });
          } else { // empty page
            resolve({
              ids: [],
              totalPages: 2,
              totalElements: 5,
              numberOfElements: 0,
              first: false,
              last: false,
              index: {},
            });
          }
        });
      };
    }

    let store;
    let paginationReducer;
    let paginationActions;

    beforeEach(() => {
      paginationReducer = makePaginationReducer();
      paginationActions = makePaginationActions(customRequestCall);
      // since fetchPage reads from state, need store to be populated
      store = mockStore({
        defaultreducer: paginationReducer(undefined, { type: 'DOES_NOT_MATTER' }),
      });
    });

    it('fetchPage should dispatch 2 simple actions when first page api action is successful', async () => {
      await store.dispatch(paginationActions.fetchPage({
        page: 0,
      }));
      const actions = store.getActions();
      // check that start and success were dispatched
      expect(actions.length).toEqual(2);
      expect(actions[0].type).toEqual('@pagination/defaultreducer/FETCH_PAGE_STARTED');
      expect(actions[1].type).toEqual('@pagination/defaultreducer/FETCH_PAGE_SUCCEEDED');
      // success action should have a payload of the first page
      console.log('ACTION 1 PAYLOAD', actions[1]);
      expect(actions[1].payload.number).toEqual(0); // page number is 0 like we requested
    });

    it('fetchPage should dispatch 2 simple actions when second page api action is successful', async () => {
      await store.dispatch(paginationActions.fetchPage({
        page: 1,
      }));
      const actions = store.getActions();
      expect(actions[1].payload.number).toEqual(1); // page number is 1 like we requested
    });

    it('fetchPage should request the first page when no page option is set', async () => {
      await store.dispatch(paginationActions.fetchPage());
      const actions = store.getActions();
      expect(actions[0].type).toEqual('@pagination/defaultreducer/FETCH_PAGE_STARTED');
      expect(actions[0].meta.pageOptions.page).toEqual(0);
    });

    it('fetchPage should store whatever comes back for a page, even if its an empty page', async () => {
      await store.dispatch(paginationActions.fetchPage({
        page: 2,
      }));
      const actions = store.getActions();
      expect(actions[1].type).toEqual('@pagination/defaultreducer/FETCH_PAGE_SUCCEEDED');
      // empty page
      expect(actions[1].payload.number).toBeUndefined();
    });

    // it('fetchPage should dispatch a failure when the api action fails', async () => {
    //   await store.dispatch(paginationActions.fetchPage({
    //     page: 0,
    //     indexed: true,
    //   }));
    //   const actions = store.getActions();
    //   expect(actions[1].payload.number).toEqual(0);
    // });
  });

  describe('Selectors for initial state', () => {
    let paginationSelectors;
    let storeState;

    beforeEach(() => {
      paginationSelectors = makePaginationSelectors();
      const paginationReducer = makePaginationReducer();
      const reducerState = paginationReducer(undefined, { type: 'DOES_NOT_MATTER' });
      storeState = {
        defaultreducer: reducerState,
      };
    });

    it('should have an empty map of pages', () => {
      expect(paginationSelectors.selectPages(storeState)).toEqual({});
    });

    it('should not have an error', () => {
      expect(paginationSelectors.selectError(storeState)).toEqual(undefined);
    });

    it('should not be loading', () => {
      expect(paginationSelectors.selectIsLoading(storeState)).toEqual(false);
    });

    it('should have no options defined', () => {
      // no options are defined, but we still keep our default page and size
      expect(paginationSelectors.selectOptions(storeState)).toEqual({
        page: 0,
        size: 10,
      });
    });
  });

  describe('Selectors for state populated with pages', () => {
    let paginationSelectors;
    let paginationActions;
    let paginationReducer;
    let storeState;
    const mockPage4 = {
      ids: ['page4result1', 'page4result2'],
      number: 4, // note this number does not matter, the PageOptions passed in determine page number resp is stored at
      totalPages: 10,
      totalElements: 20,
      numberOfElements: 2,
      first: false,
      last: false,
      index: {
        page4result1: { hello: 'page4result1' },
        page4result2: { hello: 'page4result2' },
      },
    };
    const mockPage5 = {
      ids: ['page5result1', 'page5result2'],
      number: 5,
      totalPages: 10,
      totalElements: 20,
      numberOfElements: 2,
      first: false,
      last: false,
      index: {
        page5result1: { hello: 'page5result1' },
        page5result2: { hello: 'page5result2' },
      },
    };

    beforeEach(() => {
      paginationSelectors = makePaginationSelectors();
      paginationReducer = makePaginationReducer();
      paginationActions = makePaginationActions(mockDispatchable);
      const stateAfterPage4 = paginationReducer(
        undefined,
        paginationActions.fetchPageSucceeded({
          pageOptions: { page: 4 },
          response: mockPage4,
        }),
      );
      const stateAfterPage5 = paginationReducer(
        stateAfterPage4,
        paginationActions.fetchPageSucceeded({
          pageOptions: { page: 5 },
          response: mockPage5,
        }),
      );

      storeState = {
        defaultreducer: stateAfterPage5,
      };
    });

    it('should have two pages', () => {
      const pages = paginationSelectors.selectPages(storeState) || {}; // for flow
      expect(pages['4'].response).toEqual(mockPage4);
      expect(pages['5'].response).toEqual(mockPage5);
    });

    it('should not have an error', () => {
      expect(paginationSelectors.selectError(storeState)).toEqual(undefined);
    });

    it('should not be loading', () => {
      expect(paginationSelectors.selectIsLoading(storeState)).toEqual(false);
    });

    it('should have default options defined', () => {
      expect(paginationSelectors.selectOptions(storeState)).toEqual({
        page: 0,
        size: 10,
      });
    });

    describe('when starting another request', () => {
      beforeEach(() => {
        storeState = {
          defaultreducer: paginationReducer(storeState.defaultreducer, paginationActions.fetchPageStarted({
            pageOptions: { page: 99 },
          })),
        };
      });

      it('should retain the two pages that already exist', () => {
        const pages = paginationSelectors.selectPages(storeState) || {}; // for flow
        expect(pages['4'].response).toEqual(mockPage4);
        expect(pages['5'].response).toEqual(mockPage5);
      });

      it('should be loading', () => {
        expect(paginationSelectors.selectIsLoading(storeState)).toEqual(true);
      });
    });

    describe('when encountering an error', () => {
      beforeEach(() => {
        storeState = {
          defaultreducer: paginationReducer(storeState.defaultreducer, paginationActions.fetchPageFailed({
            pageOptions: { page: 1 },
            error: { errKey: 'errValuePage1' },
          })),
        };
      });

      it('should retain the two pages that already exist', () => {
        const pages = paginationSelectors.selectPages(storeState) || {}; // for flow
        expect(pages['4'].response).toEqual(mockPage4);
        expect(pages['5'].response).toEqual(mockPage5);
      });

      it('should have an error', () => {
        // gets first error if no page specified
        expect(paginationSelectors.selectError(storeState)).toEqual({ errKey: 'errValuePage1' });
        // gets specific error if page specified
        expect(paginationSelectors.selectError(storeState, { page: 1 })).toEqual({ errKey: 'errValuePage1' });
        // no error on any other random page
        expect(paginationSelectors.selectError(storeState, { page: 0 })).toEqual(undefined);
        expect(paginationSelectors.selectError(storeState, { page: 2 })).toEqual(undefined);
        expect(paginationSelectors.selectError(storeState, { page: 3 })).toEqual(undefined);
        expect(paginationSelectors.selectError(storeState, { page: 4 })).toEqual(undefined);
        expect(paginationSelectors.selectError(storeState, { page: 5 })).toEqual(undefined);
      });

      it('should not be loading', () => {
        expect(paginationSelectors.selectIsLoading(storeState)).toEqual(false);
      });
    });
  });
});
