// @flow
import { createAction, handleActions } from 'redux-actions';

// ACTIONS
// ----------------------------------------------------------------------

// koa-pageable specific
// export const Direction = {
//   asc: 'asc',
//   desc: 'desc',
// };

// type DirectionType = $Keys<typeof Direction>;

/**
 * Sort orders for each request
 */
// type Order = {|
//   direction: DirectionType,
//   property: string,
// |};

// TODO - figure out if we want to expose these as helper types for consumers
//        using koa-pageable with redux-pageable
// koa-pageable IndexedPage
// type IndexedPage = {|
//   number: number,
//   ids: string[],
//   index: {[itemId: string]: Object}, // TODO genericize
//   totalPages: number,
//   totalElements: number,
//   numberOfElements: number,
//   first: boolean,
//   last: boolean,
// |};
//
// koa-pageable Page
// type Page = {
//   number: number,
//   content: any[],
//   totalPages: number,
//   totalElements: number,
//   numberOfElements: number,
//   first: boolean,
//   last: boolean,
// };
//
// type PageResponse = Page | IndexedPage;
//
/**
 * Specified when fetching a page.
 * These are stored for later retrieval and also passed along to the consumer supplied
 * api call method.
 */
export type PageOptions = {
  page: number,
  size?: number,
  // TODO koa-pageable specific, do we want to expose this as part of redux-pageable?
  //      could also PageOptions accept a CustomAttrs generic as well
  // orders?: Order[],
  force?: boolean,
  // not exact, so anything else can be specified here that may be needed to store
  // or to have available when the apiCallAction is dispatched
};

/**
 * The object returned from the consumers custom api call method. We suggest
 * a few properties and allow the users to specify the other properties through
 * the CustomAttrs generic.
 */
export type PageResponse<CustomAttrs: Object = {}> = {
  totalElements?: number,
  totalPages?: number,
  first?: boolean,
  last?: boolean,
  ...CustomAttrs,
  // contains whatever other payload values apiCallAction returns
};

const defaultPageOptions: PageOptions = {
  page: 0,
};

/**
 * Meta that is dispatched with each action. The redux-pageable reducer
 * uses this information to determine the pagination instance being operated on
 * and to save the pageOptions being passed along.
 */
export type Meta = {|
  pageOptions: PageOptions,
  instanceId: string,
|};

export type MetaCreatorParams = {
  pageOptions?: PageOptions,
  instanceId?: string,
};
function metaCreator(params?: MetaCreatorParams = {}): Meta {
  return {
    pageOptions: params.pageOptions || defaultPageOptions,
    instanceId: params.instanceId || 'defaultinstance',
  };
}

export type ApiCallActionParams = {|
  pageOptions: PageOptions,
  reducerId: string,
  instanceId: string,
|};

export function getActionTypes(reducerId?: string = 'defaultreducer') {
  return {
    fetchPageStartedActionType: `@pagination/${reducerId}/FETCH_PAGE_STARTED`,
    fetchPageSucceededActionType: `@pagination/${reducerId}/FETCH_PAGE_SUCCEEDED`,
    fetchPageCachedActionType: `@pagination/${reducerId}/FETCH_PAGE_CACHED`,
    fetchPageFailedActionType: `@pagination/${reducerId}/FETCH_PAGE_FAILED`,
    resetActionType: `@pagination/${reducerId}/RESET`,
    clearPagesActionType: `@pagination/${reducerId}/CLEAR_PAGES`,
    clearErrorsActionType: `@pagination/${reducerId}/CLEAR_ERRORS`,
  };
}

/**
 * Creates a set of dispatchable action creators to handle pagination.
 * @param  apiCallAction a dispatchable function that will return an async method which returns a payload for the requested page
 * @param  [reducerId]   specify a specific pagination reducer to target. the reducer must already be setup in the store configuration!
 * @return               a bunch of actions that can be dispatched
 */
export function makePaginationActions(apiCallAction: (params: ApiCallActionParams) => () => Promise<PageResponse<*>>, reducerId?: string = 'defaultreducer') {
  const { selectActivePage, selectOptions } = makePaginationSelectors(reducerId); // eslint-disable-line
  const actionTypes = getActionTypes(reducerId);
  const {
    fetchPageStartedActionType,
    fetchPageSucceededActionType,
    fetchPageCachedActionType,
    fetchPageFailedActionType,
    resetActionType,
    clearPagesActionType,
    clearErrorsActionType,
  } = actionTypes;

  // called when a pagination request is kicked off
  const fetchPageStarted: (params: MetaCreatorParams) => *
    = createAction(fetchPageStartedActionType, undefined, metaCreator);

  // called when a pagination request is successful, and passes along the response payload
  type SuccessParams = { response: PageResponse<*> };
  const fetchPageSucceeded: (params: MetaCreatorParams & SuccessParams) => *
    = createAction(
      fetchPageSucceededActionType,
      (params: SuccessParams) => params.response,
      metaCreator,
    );

  // called when a pagination request is already cached, meant to reset the fetching status
  const fetchPageCached: (params: MetaCreatorParams & SuccessParams) => *
    = createAction(
      fetchPageCachedActionType,
      (params: SuccessParams) => params.response,
      metaCreator,
    );

  // called when a pagination request fails, and passes along, as the payload, the raw error from the apiCallAction failure
  type FailParams = { error: any };
  const fetchPageFailed: (params: MetaCreatorParams & FailParams) => *
    = createAction(
      fetchPageFailedActionType,
      (params: FailParams) => params.error,
      metaCreator,
    );

  // resets particular instance state
  const reset: (params: MetaCreatorParams) => *
    = createAction(resetActionType, undefined, metaCreator);

  // clears all pages for this instance
  // user can call when they know the overall result set has changed (e.g. when sort or filters change)
  const clearPages: (params: MetaCreatorParams) => *
    = createAction(clearPagesActionType, undefined, metaCreator);

  // clears all errors for this instance
  // user can call if they have some error reconciliation process (e.g. user dismisses error)
  // note: a succesful fetch will clear an individual pages errors (e.g. recieve error then successful retry)
  const clearErrors: (params: MetaCreatorParams) => *
    = createAction(clearErrorsActionType, undefined, metaCreator);

  // TODO add page response specific actions for extra control (update page response)
  // TODO these would need to correspond with a custom supplied page response reducer

  // user calls this to request a new page
  const fetchPage = (
    pageOptions?: PageOptions = defaultPageOptions,
    instanceId?: string = 'defaultinstance',
  ) => {
    console.log(pageOptions);
    return async (dispatch: Function, getState: Function) => {
      console.log('enter');
      // get the options already set for this instance, and overlay any new options passed in
      const existingPageOptions = selectOptions(getState(), { instanceId });
      const newPageOptions: PageOptions = { ...existingPageOptions, ...pageOptions };

      dispatch(fetchPageStarted({ pageOptions: newPageOptions, instanceId }));
      console.log('started', newPageOptions);
      try {
        // check if page results already exist, dont refetch unless forced to
        let response: PageResponse<*>;
        const activePage = selectActivePage(getState(), { instanceId });
        const activePageResponse = activePage ? activePage.response : undefined;
        if (!newPageOptions.force && activePageResponse) {
          // we're able to grab the page from cache, no call needed
          console.log('pulling response from cache');
          response = activePageResponse;
          dispatch(fetchPageCached({ response, pageOptions: newPageOptions, instanceId }));
        } else {
          // dispatch the user provided action that will fetch remote data
          console.log('fresh call for response');
          response = await dispatch(apiCallAction({
            pageOptions: newPageOptions, instanceId, reducerId,
          }));
          // when forcing a page load, clear all existing pages, since it implies
          // we're expecting different data for an already successfully retrieved page
          if (newPageOptions.force) {
            dispatch(clearPages({ instanceId }));
          }
          dispatch(fetchPageSucceeded({ response, pageOptions: newPageOptions, instanceId }));
        }

        console.log('succeeded');
      } catch (error) {
        dispatch(fetchPageFailed({ error, newPageOptions, instanceId }));
        console.log('failed');
      }
    };
  };

  return {
    actionTypes,
    clearErrors,
    clearPages,
    fetchPageStarted, // should be private to redux-pageable
    fetchPageFailed, // should be private to redux-pageable
    fetchPageSucceeded, // should be private to redux-pageable
    fetchPageCached, // should be private to redux-pageable
    fetchPage, // use this one!
    reset,
  };
}

// REDUCER
// ----------------------------------------------------------------------
export type PageEntry = {|
  error?: any,
  fetching: boolean,
  pageNumber: number,
  response?: PageResponse<*>, // may not exist when a page request is started or when there is an error
|};

export type StoredPageOptions = {
  page: number, // active page
  // orders: Order[],
  size: number,
  // anything else passed in PageOptions will be stored here
}

// can't make it an exact type because of Object spread
// https://github.com/facebook/flow/issues/2405 :( :( :(
export type PaginationReducerState = {
  instances: {
    [instanceId: string]: {
      options: StoredPageOptions,
      pages: {
        [pageNumber: string]: PageEntry,
      },
      totalElements: number, // each page response updates this
      totalPages: number, // each page response updates this
    }
  }
};

function makeInitialInstanceState() {
  return {
    options: {
      page: 0,
      // orders: [],
      size: 10,
    },
    pages: {},
    totalElements: 0,
    totalPages: 0,
  };
}

const initialReducerState = {
  instances: {
    defaultinstance: makeInitialInstanceState(),
  },
};

export function makePaginationReducer(reducerId: string = 'defaultreducer') {
  return handleActions({
    [`@pagination/${reducerId}/FETCH_PAGE_STARTED`]: (state: PaginationReducerState, { meta }: { meta: Meta }): PaginationReducerState => {
      const instance = state.instances[meta.instanceId] || makeInitialInstanceState();
      // store all passed in page options except for request specific ones like force.
      const { force, ...storableOptions } = meta.pageOptions;
      const newStoredOptions = {
        ...instance.options,
        ...storableOptions,
      };

      return {
        ...state,
        instances: {
          ...state.instances,
          [meta.instanceId]: {
            ...instance,
            pages: {
              ...instance.pages,
              [meta.pageOptions.page]: {
                ...instance.pages[`${meta.pageOptions.page}`], // keep around error + existing response
                fetching: true,
                pageNumber: meta.pageOptions.page,
              },
            },
            options: newStoredOptions,
          },
        },
      };
    },
    [`@pagination/${reducerId}/FETCH_PAGE_SUCCEEDED`]: (
      state: PaginationReducerState,
      { payload, meta }: { payload: PageResponse<*>, meta: Meta },
    ): PaginationReducerState => {
      const instance = state.instances[meta.instanceId] || makeInitialInstanceState();

      return {
        ...state,
        instances: {
          ...state.instances,
          [meta.instanceId]: {
            ...instance,
            pages: {
              ...instance.pages,
              [meta.pageOptions.page]: {
                error: undefined, // reset error on fetch
                fetching: false, // fetch complete
                pageNumber: meta.pageOptions.page,
                response: payload,
              },
            },
            totalElements: payload.totalElements,
            totalPages: payload.totalPages,
          },
        },
      };
    },
    [`@pagination/${reducerId}/FETCH_PAGE_CACHED`]: (
      state: PaginationReducerState,
      { payload, meta }: { payload: PageResponse<*>, meta: Meta },
    ): PaginationReducerState => {
      const instance = state.instances[meta.instanceId] || makeInitialInstanceState();

      return {
        ...state,
        instances: {
          ...state.instances,
          [meta.instanceId]: {
            ...instance,
            pages: {
              ...instance.pages,
              [meta.pageOptions.page]: {
                ...instance.pages[`${meta.pageOptions.page}`], // keep everything else
                fetching: false, // fetch complete
              },
            },
            totalElements: payload.totalElements,
            totalPages: payload.totalPages,
          },
        },
      };
    },
    [`@pagination/${reducerId}/FETCH_PAGE_FAILED`]: (state: PaginationReducerState, { payload, meta }: { payload: any, meta: Meta }): PaginationReducerState => {
      const instance = state.instances[meta.instanceId] || makeInitialInstanceState();

      return {
        ...state,
        instances: {
          ...state.instances,
          [meta.instanceId]: {
            ...instance,
            pages: {
              ...instance.pages,
              [meta.pageOptions.page]: {
                error: payload, // error for this specific page
                fetching: false, // fetch complete,
                pageNumber: meta.pageOptions.page,
              },
            },
          },
        },
      };
    },
    // TODO add tests
    [`@pagination/${reducerId}/RESET`]: (state: PaginationReducerState, { meta }: { meta: Meta }): PaginationReducerState =>
      ({
        ...state,
        instances: {
          ...state.instances,
          [meta.instanceId]: makeInitialInstanceState(),
        },
      }),
    // TODO add tests
    [`@pagination/${reducerId}/CLEAR_ERRORS`]: (state: PaginationReducerState, { meta }: { meta: Meta }): PaginationReducerState => {
      const instance = state.instances[meta.instanceId] || makeInitialInstanceState();

      return {
        ...state,
        instances: {
          ...state.instances,
          [meta.instanceId]: {
            ...instance,
            pages: {
              ...Object.keys(instance.pages)
                .reduce((updatedPages, key) => {
                  // grab everything except the error, and set that as the page entry
                  const { error, ...rest } = instance.pages[key];
                  updatedPages[key] = rest; // eslint-disable-line
                  return updatedPages;
                }, {}),
            },
          },
        },
      };
    },
    // TODO add tests
    [`@pagination/${reducerId}/CLEAR_PAGES`]: (state: PaginationReducerState, { meta }: { meta: Meta }): PaginationReducerState => {
      const instance = state.instances[meta.instanceId] || makeInitialInstanceState();

      return {
        ...state,
        instances: {
          ...state.instances,
          [meta.instanceId]: {
            ...instance,
            pages: {},
          },
        },
      };
    },
  }, initialReducerState);
}

// SELECTORS
// ----------------------------------------------------------------------
export type SelectorProps = {
  instanceId?: string,
  page?: number, // valid for page specific selectors
};

type StoreState = {
  [reducerId: string]: PaginationReducerState,
};

export type Totals = {|
  totalElements: number,
  totalPages: number,
|};

function getInstanceIdFromProps(props: ?Object) {
  if (props && props.instanceId) {
    return props.instanceId;
  }
  return 'defaultinstance';
}

export function makePaginationSelectors(reducerId: string = 'defaultreducer') {
  const selectPages = (state: StoreState, props?: SelectorProps): ?{[pageNumber: string]: PageEntry} => {
    // console.log('selecting pages', { reducerId, props, state });
    const instanceId = getInstanceIdFromProps(props);
    const instance = state[reducerId].instances[instanceId];
    return instance ? instance.pages : undefined;
  };

  const selectActivePage = (state: StoreState, props?: SelectorProps): ?PageEntry => {
    // console.log('selecting active page', { reducerId, props, state });
    const instanceId = getInstanceIdFromProps(props);
    const instance = state[reducerId].instances[instanceId];
    const { page } = instance.options;
    return instance ? instance.pages[`${page}`] : undefined;
  };

  const selectOptions = (state: StoreState, props?: SelectorProps): ?StoredPageOptions => {
    // console.log('selecting options', { reducerId, props, state });
    const instanceId = getInstanceIdFromProps(props);
    const instance = state[reducerId].instances[instanceId];
    return instance ? instance.options : undefined;
  };

  const selectActivePageNumber = (state: StoreState, props?: SelectorProps): number => {
    const options = selectOptions(state, props);
    return options ? options.page : 0;
  };

  const selectIsLoading = (state: StoreState, props?: SelectorProps): boolean => {
    // console.log('selecting is loading', { reducerId, props, state });
    const instanceId = getInstanceIdFromProps(props);
    const instance = state[reducerId].instances[instanceId];
    const page = props && props.page !== undefined ? props.page.toString() : undefined;
    if (!instance) {
      return false;
    }
    // Are we looking for a specific page fetching status?
    if (page) {
      const pageEntry = instance.pages[page];
      return pageEntry ? pageEntry.fetching : false;
    }
    // Is any page fetching?
    const pageEntries: PageEntry[] = (Object.values(instance.pages): any); // must cast through any
    return pageEntries.some(pageEntry => pageEntry.fetching);
  };

  const selectError = (state: StoreState, props?: SelectorProps): ?any => {
    // console.log('selecting is loading', { reducerId, props, state });
    const instanceId = getInstanceIdFromProps(props);
    const instance = state[reducerId].instances[instanceId];
    const page = props && props.page !== undefined ? props.page.toString() : undefined;
    if (!instance) {
      return undefined;
    }
    // Are we looking for a specific page fetch error?
    if (page) {
      const pageEntry = instance.pages[page];
      return pageEntry ? pageEntry.error : undefined;
    }
    // If any page has an error, return the first matching error
    const pageEntries: PageEntry[] = (Object.values(instance.pages): any);
    const firstEntryWithError: ?PageEntry = pageEntries.find(pageEntry => !!pageEntry.error);
    return firstEntryWithError ? firstEntryWithError.error : undefined;
  };

  const selectTotals = (state: StoreState, props?: SelectorProps): ?Totals => {
    // console.log('selecting options', { reducerId, props, state });
    const instanceId = getInstanceIdFromProps(props);
    const instance = state[reducerId].instances[instanceId];
    return instance ? {
      totalElements: instance.totalElements,
      totalPages: instance.totalPages,
    } : undefined;
  };

  return {
    selectActivePage,
    selectActivePageNumber,
    selectPages,
    selectOptions,
    selectIsLoading,
    selectError,
    selectTotals,
  };
}
